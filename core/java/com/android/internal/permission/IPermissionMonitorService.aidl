package com.android.internal.permission;
/*
 * {@hide}
 */
interface IPermissionMonitorService {
    boolean setSetting(String packageName, String permSuspect, boolean set);
    boolean updateSettings(String packageName, in String[] permSuspects);
    String[] getSettings(String packageName);
}

