package com.android.internal.permission;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

public class PermissionMonitor {
    
    private static final String TAG = "PermissionMonitor";

    public static final String LOG_NAME = "permission-monitor.log";
    
    private IPermissionMonitorService mService;
    
    private Context mContext;
    
    public PermissionMonitor(Context ctx, IPermissionMonitorService service) {
        mContext = ctx;
        mService = service;
    }

    public boolean setSetting(String packageName, String permSuspect, boolean set){
        boolean ret = false;
        try {
            ret = mService.setSetting(packageName, permSuspect, set);
        } catch (RemoteException e) {
            Log.e(TAG, "setSetting failed");
        }
        return ret;
    }
  

    public boolean updateSettings(String packageName, String[] permSuspects) {
        boolean ret = false;
        try {
            ret = mService.updateSettings(packageName, permSuspects);
        } catch (RemoteException e) {
            Log.e(TAG, "updateSettings failed");
        }
        return ret;
    }

    public String[] getSettings(String packageName) {
        String[] ret = null;
        try {
            ret = mService.getSettings(packageName);
        } catch (RemoteException e) {
            Log.e(TAG, "getSettings failed");
        }
        return ret;
    }
}
