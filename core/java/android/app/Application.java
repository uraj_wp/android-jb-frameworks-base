/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import java.util.ArrayList;

import android.content.ComponentCallbacks;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Bundle;

// droid-perm add
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.android.internal.permission.PermissionMonitor;

import java.util.HashSet;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
// droid-perm end

/**
 * Base class for those who need to maintain global application state. You can
 * provide your own implementation by specifying its name in your
 * AndroidManifest.xml's &lt;application&gt; tag, which will cause that class
 * to be instantiated for you when the process for your application/package is
 * created.
 * 
 * <p class="note">There is normally no need to subclass Application.  In
 * most situation, static singletons can provide the same functionality in a
 * more modular way.  If your singleton needs a global context (for example
 * to register broadcast receivers), the function to retrieve it can be
 * given a {@link android.content.Context} which internally uses
 * {@link android.content.Context#getApplicationContext() Context.getApplicationContext()}
 * when first constructing the singleton.</p>
 */
public class Application extends ContextWrapper implements ComponentCallbacks2 {
    private ArrayList<ComponentCallbacks> mComponentCallbacks =
            new ArrayList<ComponentCallbacks>();
    private ArrayList<ActivityLifecycleCallbacks> mActivityLifecycleCallbacks =
            new ArrayList<ActivityLifecycleCallbacks>();

    /** @hide */
    public LoadedApk mLoadedApk;

    public interface ActivityLifecycleCallbacks {
        void onActivityCreated(Activity activity, Bundle savedInstanceState);
        void onActivityStarted(Activity activity);
        void onActivityResumed(Activity activity);
        void onActivityPaused(Activity activity);
        void onActivityStopped(Activity activity);
        void onActivitySaveInstanceState(Activity activity, Bundle outState);
        void onActivityDestroyed(Activity activity);
    }

    public Application() {
        super(null);
    }

    /**
     * Called when the application is starting, before any other application
     * objects have been created.  Implementations should be as quick as
     * possible (for example using lazy initialization of state) since the time
     * spent in this function directly impacts the performance of starting the
     * first activity, service, or receiver in a process.
     * If you override this method, be sure to call super.onCreate().
     */
    public void onCreate() {
    }

    /**
     * This method is for use in emulated process environments.  It will
     * never be called on a production Android device, where processes are
     * removed by simply killing them; no user code (including this callback)
     * is executed when doing so.
     */
    public void onTerminate() {
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Object[] callbacks = collectComponentCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ComponentCallbacks)callbacks[i]).onConfigurationChanged(newConfig);
            }
        }
    }

    public void onLowMemory() {
        Object[] callbacks = collectComponentCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ComponentCallbacks)callbacks[i]).onLowMemory();
            }
        }
    }

    public void onTrimMemory(int level) {
        Object[] callbacks = collectComponentCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                Object c = callbacks[i];
                if (c instanceof ComponentCallbacks2) {
                    ((ComponentCallbacks2)c).onTrimMemory(level);
                }
            }
        }
    }

    public void registerComponentCallbacks(ComponentCallbacks callback) {
        synchronized (mComponentCallbacks) {
            mComponentCallbacks.add(callback);
        }
    }

    public void unregisterComponentCallbacks(ComponentCallbacks callback) {
        synchronized (mComponentCallbacks) {
            mComponentCallbacks.remove(callback);
        }
    }

    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        synchronized (mActivityLifecycleCallbacks) {
            mActivityLifecycleCallbacks.add(callback);
        }
    }

    public void unregisterActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        synchronized (mActivityLifecycleCallbacks) {
            mActivityLifecycleCallbacks.remove(callback);
        }
    }
    
    // ------------------ Internal API ------------------
    
    /**
     * @hide
     */
    /* package */ final void attach(Context context) {
        attachBaseContext(context);
        mLoadedApk = ContextImpl.getImpl(context).mPackageInfo;
        if (context != null)
             initPermMonSet();
    }

    /* package */ void dispatchActivityCreated(Activity activity, Bundle savedInstanceState) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityCreated(activity,
                        savedInstanceState);
            }
        }
    }

    /* package */ void dispatchActivityStarted(Activity activity) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityStarted(activity);
            }
        }
    }

    /* package */ void dispatchActivityResumed(Activity activity) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityResumed(activity);
            }
        }
    }

    /* package */ void dispatchActivityPaused(Activity activity) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityPaused(activity);
            }
        }
    }

    /* package */ void dispatchActivityStopped(Activity activity) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityStopped(activity);
            }
        }
    }

    /* package */ void dispatchActivitySaveInstanceState(Activity activity, Bundle outState) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivitySaveInstanceState(activity,
                        outState);
            }
        }
    }

    /* package */ void dispatchActivityDestroyed(Activity activity) {
        Object[] callbacks = collectActivityLifecycleCallbacks();
        if (callbacks != null) {
            for (int i=0; i<callbacks.length; i++) {
                ((ActivityLifecycleCallbacks)callbacks[i]).onActivityDestroyed(activity);
            }
        }
    }

    private Object[] collectComponentCallbacks() {
        Object[] callbacks = null;
        synchronized (mComponentCallbacks) {
            if (mComponentCallbacks.size() > 0) {
                callbacks = mComponentCallbacks.toArray();
            }
        }
        return callbacks;
    }

    private Object[] collectActivityLifecycleCallbacks() {
        Object[] callbacks = null;
        synchronized (mActivityLifecycleCallbacks) {
            if (mActivityLifecycleCallbacks.size() > 0) {
                callbacks = mActivityLifecycleCallbacks.toArray();
            }
        }
        return callbacks;
    }

// droid-perm add
    private HashSet<String> mPermMonSet = new HashSet<String>();

    private void initPermMonSet() {
        String thisPackage = getPackageName();
        Log.i("droid-perm", "Init permission monitor settings for " + thisPackage);

        try {
            ApplicationInfo ai = getPackageManager().
                getApplicationInfo(thisPackage, PackageManager.GET_META_DATA);
            if ((ai.uid < 10000) || // system apps' uids are less than 10000 (always true?)
                (((ApplicationInfo.FLAG_SYSTEM |
                   ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) & ai.flags) != 0)) {
                Log.i("droid-perm", "Skip package " + thisPackage);
                return;
            }
        } catch (NameNotFoundException e) {
            Log.e("droid-perm", "No package info for " + thisPackage);
            return;
        } catch (NullPointerException e) {
            Log.e("droid-perm", "PackageManagerService not available");
            return;
        }
        
        PermissionMonitor permMon = null;
        try {
            permMon = (PermissionMonitor)getSystemService(Context.PERMISSION_MONITOR_SERVICE);
        } catch (java.lang.Throwable E) {
          Log.e("droid-perm", "No valid base for this ContextWrapper:" + thisPackage);
          return;
        }

        String[] permSuspects = permMon.getSettings(thisPackage);
        if (permSuspects.length == 0) {
            return;
        }

        for (int i = 0; i < permSuspects.length; ++i) {
            mPermMonSet.add(permSuspects[i]);
			// add guopy
			Log.i("droid-perm", "Registered permission for " + thisPackage + ": " + permSuspects[i]);
			// end guopy
        }

        // This is the application startup, so we do not need a lock here;
        FileOutputStream fos;
        try {
            fos = openFileOutput(PermissionMonitor.LOG_NAME, Context.MODE_APPEND);
            fos.write(("#" + System.currentTimeMillis() + "\n").getBytes());
            fos.close();
            Log.i("droid-perm", "Write init log for: " + thisPackage);
        } catch (Exception e) {
            Log.e("droid-perm", "Cannot init log: " + thisPackage, e);
        }
    }

    private Object mLogLock = new Object();
    
    /**
     * {@hide}
     */
    public void logPermissionRequest(String permission) {
		// add guopy
		Log.i("droid-perm", "logPermissionRequest for " + getPackageName() + ": " + permission);
		// end guopy
        if(!mPermMonSet.contains(permission)) {
            return;
        }
		// add guopy
		Log.i("droid-perm", "Log succeed for " + getPackageName() + ": " + permission);
		// end guopy
        synchronized (mLogLock) {
            FileOutputStream fos;
            try {
                fos = openFileOutput(PermissionMonitor.LOG_NAME, Context.MODE_APPEND);
            } catch (FileNotFoundException e) {
                Log.e("droid-perm", "Cannot open log file: " + getPackageName(), e);
                return;
            }
            
            StackTraceElement[] stes = Thread.currentThread().getStackTrace();
            long time = System.currentTimeMillis();
            StringBuilder log = new StringBuilder(String.valueOf(time) + ";" + permission + ";");
            // We want to skip the first 3 trivial stacks
            for (int i = 3; i < stes.length; ++i) {
                log.append(stes[i].getMethodName());
                log.append("@");
                log.append(stes[i].getClassName());
                log.append(";");
            }
            log.append("\n");

            try {
                fos.write(log.toString().getBytes());
                fos.close();
            } catch (IOException e) {
                Log.e("droid-perm", "IO error: " + getPackageName(), e);
            }
            return;
        }
    }
    // droid-perm end
	
	// droid-pot add
	/**
	 * {@hide}
	 */
	public void logProbingRequests(String functionname) {
		if (!(getPackageName().equals("com.android.musicfx") || getPackageName().equals("com.android.launcher")))
			Log.i("droid-pot", "Probing action happened in " + getPackageName() + " by using function " + functionname);
	}
	// droid-pot end
}
