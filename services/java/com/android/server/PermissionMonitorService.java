package com.android.server;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Slog;

import com.android.internal.permission.IPermissionMonitorService;

import java.io.File;
import java.io.IOException;

public class PermissionMonitorService extends IPermissionMonitorService.Stub {

    private static final String TAG = "PermissionMonitorService";

    private Context mContext;
    private SQLiteDatabase mDB;

    public PermissionMonitorService(Context context) {
        super();
        mDB = null;
        mContext = context;
        if (!prepareDatabase()) {
            throw new RuntimeException("Cannot open permission monitor database");
        }
    }
    
    private static final String SYSDATA_DIR = "/data/system/";
    private static final String DATABASE_FILE = SYSDATA_DIR + "pm.db";
    private static final long DATABASE_VERSION = 1;

    private static final String TABLE_VERSION = "version";
    private static final String TABLE_SETTINGS = "settings";
    
    private static final String CREATE_TABLE_VERSION = 
        "CREATE TABLE IF NOT EXISTS  " + TABLE_VERSION + " (" +
        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "version INTEGER);";
    
    private static final String INSERT_VERSION =
        "INSERT OR REPLACE INTO " + TABLE_VERSION +
        " (version) VALUES (" + DATABASE_VERSION + ");";
    
    private static final String CREATE_TABLE_SETTINGS = 
        "CREATE TABLE IF NOT EXISTS  " + TABLE_SETTINGS + " (" +
        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
        "packageName TEXT, " +
        "permSuspect TEXT);";

    public synchronized boolean prepareDatabase() {
        boolean canWrite = new File(SYSDATA_DIR).canWrite();
        if (canWrite) {
            File dbFile = new File(DATABASE_FILE);
            if (!dbFile.exists()) {
                try {
                    dbFile.createNewFile();
                } catch (IOException e) {
                    Slog.e(TAG, "Cannot create database file");
                    return false;
                }
            }
            try {
                 mDB = SQLiteDatabase.openDatabase(DATABASE_FILE, null,
                                                   SQLiteDatabase.OPEN_READWRITE | 
                                                   SQLiteDatabase.CREATE_IF_NECESSARY);
                 mDB.execSQL(CREATE_TABLE_VERSION);
                 mDB.execSQL(INSERT_VERSION);
                 mDB.execSQL(CREATE_TABLE_SETTINGS);
            } catch (SQLException e) {
                 Slog.e(TAG, "Database creation failed.", e);
                 return false;
            }
        } else {
            Slog.d(TAG, "Cannot write to " + SYSDATA_DIR);
            return false;
        }
        return true;
    }
    
    public boolean updateSettings(String packageName, String[] permSuspects) {
        boolean ret = false;
        ContentValues cv = new ContentValues();
        cv.put("packageName", packageName);
        mDB.beginTransaction();
        try {
            mDB.delete(TABLE_SETTINGS,
                       "packageName=\"?\"",
                       new String[] { packageName });
            for (int i = 0; i < permSuspects.length; ++i) {
                cv.put("permSuspect", permSuspects[i]);
                mDB.insert(TABLE_SETTINGS, null, cv);
            }
            mDB.setTransactionSuccessful();
            ret = true;
        } finally {
            mDB.endTransaction();
        }
        return ret;
    }

    public boolean setSetting(String packageName, String permSuspect, boolean set) {
        boolean ret = false;
        mDB.beginTransaction();
        try {
            mDB.delete(TABLE_SETTINGS, 
                       "packageName=\"?\" and permSuspect=\"?\"",
                       new String[] { packageName, permSuspect });
            if (set) { // insert
                ContentValues cv = new ContentValues();
                cv.put("packageName", packageName);
                cv.put("permSuspect", permSuspect);
                mDB.insert(TABLE_SETTINGS, null, cv);
            }
            mDB.setTransactionSuccessful();
            ret = true;
        } finally {
            mDB.endTransaction();
        }
        return ret;
    }
    
    public String[] getSettings(String packageName) {
        final String[] col = new String[] {"permSuspect"};
        
        String[] ret = null;
        Cursor c = mDB.query(TABLE_SETTINGS, col, 
                             "packageName=\""+packageName+"\"",
                             null, null, null, null);
        ret = new String[c.getCount()];
        int row = 0;
        while(c.moveToNext()) {
            ret[row] = c.getString(0);
            ++row;
        }
        c.close();
        return ret;
    }

    protected void finalize() throws java.lang.Throwable {
        super.finalize();
        if (mDB != null)
            mDB.close();
    }
}
